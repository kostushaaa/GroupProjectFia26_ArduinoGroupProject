#include <Arduino.h>
#include "TM1637.h"
#include "LiquidCrystal_I2C.h"


const int LED_RED = 4;
const int LED_GREEN = 5;
const int LED_BLUE = 6;
const int POTENTIOMETER_PIN = A0;
const int CLK_PIN = 10;
const int DIO_PIN = 11;
const int THERMOMETER_PIN = A1;
const int BUZZER_PIN = 3;


const int MIN_TEMP = 20;
const int MAX_TEMP = 24;

bool isDisplayConnected = false;


TM1637 tm(CLK_PIN, DIO_PIN);
LiquidCrystal_I2C lcd(0x27, 16, 2);
int internal_max_temp = MAX_TEMP;


void setup() {
    pinMode(LED_RED, OUTPUT);
    pinMode(LED_GREEN, OUTPUT);
    pinMode(LED_BLUE, OUTPUT);
    pinMode(POTENTIOMETER_PIN, INPUT);
    Serial.begin(115200);
    tm.init();
    tm.set(BRIGHT_TYPICAL);
    pinMode(THERMOMETER_PIN, INPUT);
    pinMode(BUZZER_PIN, OUTPUT);

    if (isDisplayConnected) {
        lcd.init();
        lcd.backlight();
        lcd.setCursor(0, 0);
        lcd.print("HACK THE");
        lcd.setCursor(0, 1);
        lcd.print("PLANET!!!!!");
        delay(4500);
        lcd.clear();
    }
}


// Promissses
void turnOnRedLED();
void turnOnGreenLED();
void turnOnBlueLED();
void printFourDigits(int counter);
float readTemperature();
int calculatePotentiometerPercentage(int value);
int calculateMaxTempByPotentiometer(int percentage);
void lcdPrintTemps(int minTemp, int maxTemp);


void loop() {
    int percentage = calculatePotentiometerPercentage(analogRead(POTENTIOMETER_PIN));
    printFourDigits(percentage);

    float celsius = readTemperature();

    if (celsius < MIN_TEMP) {
        turnOnBlueLED();
    } else if (celsius > internal_max_temp) {
        turnOnRedLED();
    } else {
        turnOnGreenLED();
    }

    internal_max_temp = calculateMaxTempByPotentiometer(percentage);
    if (isDisplayConnected) {
        lcdPrintTemps(MIN_TEMP, internal_max_temp);
    }
}

void turnOnRedLED() {
    digitalWrite(LED_GREEN, LOW);
    digitalWrite(LED_RED, HIGH);
    digitalWrite(LED_BLUE, LOW);
}

void turnOnGreenLED() {
    digitalWrite(LED_RED, LOW);
    digitalWrite(LED_BLUE, LOW);
    digitalWrite(LED_GREEN, HIGH);
}

void turnOnBlueLED() {
    digitalWrite(LED_RED, LOW);
    digitalWrite(LED_GREEN, LOW);
    digitalWrite(LED_BLUE, HIGH);
}

void printFourDigits(int counter) {
    tm.display(0, (counter / 1000) % 10);
    tm.display(1, (counter / 100) % 10);
    tm.display(2, (counter / 10) % 10);
    tm.display(3, counter % 10);
}

float readTemperature() {
    const float BETA = 3950;
    int analogValue = analogRead(THERMOMETER_PIN);
    float celsius = 1 / (log(1 / (1023.0 / analogValue - 1)) / BETA + 1.0 / 298.15) - 273.15;
    return celsius;
}

int calculatePotentiometerPercentage(int value) {
    float percentage = (float)value / 1023.0 * 100.0;
    int roundedPercentage = round(percentage);
    return roundedPercentage;
}

int calculateMaxTempByPotentiometer(int percentage) {
    return MAX_TEMP + ((MAX_TEMP - MIN_TEMP) * percentage) / 100;
}

void lcdPrintTemps(int minTemp, int maxTemp) {
    lcd.setCursor(0, 0);
    lcd.print("Min : " + String(minTemp) + " C");
    lcd.setCursor(0, 1);
    lcd.print("Max : " + String(maxTemp) + " C");
}
